<?php

namespace TJVB\Testreportmixer\Tests;

use PHPUnit\Framework\TestCase as PHPUnitTestCase;

/**
 * The base TestCase for all the tests we have
 *
 * @author Tobias van Beek <t.vanbeek@tjvb.nl>
 */
abstract class TestCase extends PHPUnitTestCase
{
    protected $dataDir = __DIR__ . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR;
    protected $tempDir = __DIR__ . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'temp'. DIRECTORY_SEPARATOR;

    /**
     * Test if a string is valid xml
     *
     * @param string $possibleXml
     *
     * @throws AssertionFailedError
     */
    public function assertXmlIsValid(string $possibleXml)
    {
        $xml = \simplexml_load_string($possibleXml);
        $this->assertNotFalse($xml, $possibleXml . ' isn\'t a valid xml');
    }
}