<?php

namespace TJVB\Testreportmixer\Tests\Exceptions;

use TJVB\Testreportmixer\Tests\TestCase;
use TJVB\Testreportmixer\Exceptions\FileAccessException;

/**
 * Test the FileAccessEception
 *
 * @author Tobias van Beek <t.vanbeek@tjvb.nl>
 *
 * @group exceptions
 */
class FileAccessExceptionTest extends TestCase
{
    /**
     * The FileAccessException can be throwd
     *
     * @tests
     */
    public function it_can_be_throwd()
    {
        $this->expectException(FileAccessException::class);
        throw new FileAccessException();
    }

    /**
     * Test that we get a correct no read access message
     *
     * @test
     */
    public function it_can_throw_a_no_read_access_message()
    {
        $filename = 'blaat' . \mt_rand();
        $this->expectException(FileAccessException::class);
        $this->expectExceptionMessageRegExp('/can\'t read (.)* '.$filename.'/');
        throw FileAccessException::noReadAccess($filename);
    }

    /**
     * Test that we get a correct no write access message
     *
     * @test
     */
    public function it_can_throw_a_no_write_access_message()
    {
        $filename = 'blaat' . \mt_rand();
        $this->expectException(FileAccessException::class);
        $this->expectExceptionMessageRegExp('/can\'t write (.)* '.$filename.'/');
        throw FileAccessException::noWriteAccess($filename);
    }
}