<?php

namespace TJVB\Testreportmixer\Tests\Exceptions\Parse;

use TJVB\Testreportmixer\Tests\TestCase;
use TJVB\Testreportmixer\Exceptions\Parse\XmlException;

/**
 * Test the XmlException
 *
 * @author Tobias van Beek <t.vanbeek@tjvb.nl>
 *
 * @group exceptions
 */
class XmlExceptionTest extends TestCase
{
    /**
     * The XmlException can be throwd
     *
     * @tests
     */
    public function it_can_be_throwd()
    {
        $this->expectException(XmlException::class);
        throw new XmlException();
    }

    /**
     * Test that we will get the fatal message
     *
     * @tests
     */
    public function it_will_handle_the_fatal_case()
    {
        $error = $this->getXmlError();
        $error->level = LIBXML_ERR_FATAL;
        $errors = [$error];

        $this->expectException(XmlException::class);
        $this->expectExceptionMessageRegExp('/FATAL: /');
        throw XmlException::xmlerrors($errors);
    }

    /**
     * Test that we will get the error message
     *
     * @tests
     */
    public function it_will_handle_the_error_case()
    {
        $error = $this->getXmlError();
        $error->level = LIBXML_ERR_ERROR;
        $errors = [$error];

        $this->expectException(XmlException::class);
        $this->expectExceptionMessageRegExp('/ERROR: /');
        throw XmlException::xmlerrors($errors);
    }

    /**
     * Test that we will get the warning message
     *
     * @tests
     */
    public function it_will_handle_the_warning_case()
    {
        $error = $this->getXmlError();
        $error->level = LIBXML_ERR_WARNING;
        $errors = [$error];

        $this->expectException(XmlException::class);
        $this->expectExceptionMessageRegExp('/WARNING: /');
        throw XmlException::xmlerrors($errors);
    }

    /**
     * Test that we will get the unknown message
     *
     * @tests
     */
    public function it_will_handle_the_unknown_case()
    {
        $error = $this->getXmlError();
        $error->level = -1;
        $errors = [$error];

        $this->expectException(XmlException::class);
        $this->expectExceptionMessageRegExp('/UNKNOWN: /');
        throw XmlException::xmlerrors($errors);
    }

    /**
     * Test that we will get the unknown message
     *
     * @tests
     */
    public function it_will_handle_the_noxmlerror_case()
    {
        $error = new \stdClass();
        $errors = [$error];

        $this->expectException(XmlException::class);
        $this->expectExceptionMessageRegExp('/UNKNOWN ERROR: /');
        throw XmlException::xmlerrors($errors);
    }

    /**
     * Get an xml error to use
     *
     * @return \LibXMLError
     */
    protected function getXmlError()
    {
        $error = new \LibXMLError();
        $error->code = 1;
        $error->column = 1;
        $error->level = 1;
        $error->line = 1;
        $error->message = '';
        return $error;
    }
}