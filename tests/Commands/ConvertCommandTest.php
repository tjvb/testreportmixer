<?php

namespace TJVB\Testreportmixer\Tests\Commands;

use org\bovigo\vfs\vfsStream;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Application;
use TJVB\Testreportmixer\Commands\ConvertCommand;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * The test to test the convert command
 *
 * @author Tobias van Beek (TJVB) <t.vanbeek@tjvb.nl>
 */
class ConvertCommandTest extends TestCase
{

    /**
     * Test the execute command
     *
     * @tests
     */
    public function it_can_register_the_command()
    {
        $application = new Application();
        $application->add(new ConvertCommand());

        $command = $application->find('convert');
        $commandTester = new CommandTester($command);

        // we just have register it
        $this->assertTrue(true);
    }

    /**
     * It will handle an error if the parser (input format) is not found
     *
     * @tests
     */
    public function it_will_handle_a_not_found_parser()
    {
        $application = new Application();
        $application->add(new ConvertCommand());

        $command = $application->find('convert');
        $commandTester = new CommandTester($command);

        $parameters = $this->getDummyParameters();
        $parameters['inputformat'] = 'i_dont_exist';

        $commandTester->execute($parameters);
        $output = $commandTester->getDisplay();

        $this->assertContains('Parser', $output);
        $this->assertContains('i_dont_exist', $output);
    }

    /**
     * It will handle an error if the writer (output format) is not found
     *
     * @tests
     */
    public function it_will_handle_a_not_found_writer()
    {
        $application = new Application();
        $application->add(new ConvertCommand());

        $command = $application->find('convert');
        $commandTester = new CommandTester($command);

        $parameters = $this->getDummyParameters();
        $parameters['outputformat'] = 'i_dont_exist';

        $commandTester->execute($parameters);
        $output = $commandTester->getDisplay();

        $this->assertContains('Writer', $output);
        $this->assertContains('i_dont_exist', $output);
    }

    /**
     * It will handle an error if the parser (input format) failed to parse
     *
     * @tests
     */
    public function it_will_handle_a_parser_problem()
    {
        $application = new Application();
        $application->add(new ConvertCommand());

        $command = $application->find('convert');
        $commandTester = new CommandTester($command);

        $parameters = $this->getDummyParameters();
        // we use a real parser that will give an error because the file is empty
        $parameters['inputformat'] = 'junit';

        $commandTester->execute($parameters);
        $output = $commandTester->getDisplay();

        $this->assertContains('Failed to parse ', $output);
    }

    /**
     * It will handle an error if the writer (output format) failed to write
     *
     * @tests
     */
    public function it_will_handle_failed_to_writer()
    {
        $application = new Application();
        $application->add(new ConvertCommand());

        $command = $application->find('convert');
        $commandTester = new CommandTester($command);

        $root = vfsStream::setup('testdir', 0000)->chown(vfsStream::OWNER_ROOT);
        $file = vfsStream::newFile('demo', 0000)->chown(vfsStream::OWNER_ROOT)->at($root);

        $parameters = $this->getDummyParameters();
        // we use a real writer that will give an error because we can't write the file
        $parameters['outputfile'] = vfsStream::url('testdir/demo');
        $parameters['outputformat'] = 'junit';

        $commandTester->execute($parameters);
        $output = $commandTester->getDisplay();

        $this->assertContains('Failed to write', $output);
    }

    /**
     * it will execute if all parameters are valid
     *
     * @tests
     */
    public function it_will_execute()
    {
        $application = new Application();
        $application->add(new ConvertCommand());

        $command = $application->find('convert');
        $commandTester = new CommandTester($command);

        $parameters = $this->getDummyParameters();

        $commandTester->execute($parameters);
        $output = $commandTester->getDisplay();

        $this->assertContains('Finished', $output);
    }

    /**
     * Get dummy parameters to execute the convert command
     *
     * @return string[]
     */
    protected function getDummyParameters()
    {
        $tempOutFile = tempnam(sys_get_temp_dir(), 'output');
        $tempInFile = tempnam(sys_get_temp_dir(), 'input');

        return [
            'outputformat' => '\\TJVB\\Testreportmixer\\Tests\\Stubs\\WriterStub',
            'outputfile' => $tempOutFile,
            'inputformat' => '\\TJVB\\Testreportmixer\\Tests\\Stubs\\ParserStub',
            'inputfiles' => [$tempInFile],
        ];
    }
}
