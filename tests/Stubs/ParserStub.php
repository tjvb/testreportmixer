<?php

namespace TJVB\Testreportmixer\Tests\Stubs;

use TJVB\Testreportmixer\Parsers\ParserInterface;

/**
 * A test parser
 *
 * @author Tobias van Beek (TJVB) <t.vanbeek@tjvb.nl>
 */
class ParserStub implements ParserInterface
{
    /**
     * Parse the files
     *
     * @param string $filepath
     * @param array $options
     *
     * @throws ParseException
     */
    public function parseFile(string $filepath, array $options = array()): bool
    {
        return false;
    }
    /**
     * Get the number of assertions
     *
     * @return int
     */
    public function assertions(): int
    {
        return 0;
    }
    /**
     * Get the number of errors test
     *
     * @return int
     */
    public function errors(): int
    {
        return 0;
    }
    /**
     * Get the number of failures tests
     *
     * @return int
     */
    public function failures(): int
    {
        return 0;
    }
    /**
     * Get the number of skipped tests
     *
     * @return int
     */
    public function skipped(): int
    {
        return 0;
    }

    /**
     * Get the number of tests
     *
     * @return int
     */
    public function tests(): int
    {
        return 0;
    }

    /**
     * Get the testfiles
     *
     * @return array
     */
    public function getTestFiles(): array
    {
        return [];
    }
}
