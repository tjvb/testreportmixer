<?php

namespace TJVB\Testreportmixer\Tests\Stubs;

use TJVB\Testreportmixer\Writers\WriterInterface;

/**
 * A test writer
 *
 * @author Tobias van Beek (TJVB) <t.vanbeek@tjvb.nl>
 */
class WriterStub implements WriterInterface
{
    /**
     * Add the testfiles to the writer
     *
     * @param array $testfiles
     */
    public function addTestFiles(array $testfiles)
    {
        // do nothing
    }

    /**
     * The function to write the data to a file
     *
     * @param string $path
     *
     * @throws WriteException
     *
     * @return bool
     */
    public function writeToFile(string $path) : bool
    {
        return true;
    }

    /**
     * The function to write the data to a directory
     *
     * @param string $path
     *
     * @throws WriteException
     *
     * @return bool
     */
    public function writeToDir(string $path) : bool
    {
        return true;
    }
}
