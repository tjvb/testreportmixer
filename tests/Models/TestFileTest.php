<?php
namespace TJVB\Testreportmixer\Tests\Models;

use TJVB\Testreportmixer\Tests\TestCase;
use TJVB\Testreportmixer\Models\TestFile;
use TJVB\Testreportmixer\Models\Interfaces\TestFileInterface;

/**
 * Test the TestFile model
 *
 * @author Tobias van Beek <t.vanbeek@tjvb.nl>
 *
 * @group moddels
 */
class TestFileTest extends TestCase
{
    /**
     * Check the instance of the interface
     *
     * @tests
     */
    public function it_is_instance_of_the_interface()
    {
        $testfile = new TestFile();
        $this->assertInstanceOf(TestFile::class, $testfile);
        $this->assertInstanceOf(TestFileInterface::class, $testfile);
    }

    /**
     * Test the file function without a parameter
     *
     * @tests
     */
    public function it_gives_an_empty_file_if_not_set()
    {
        $testfile = new TestFile();
        $result = $testfile->file();
        $this->assertInternalType('string', $result);
        $this->assertEquals('', $result);
    }

    /**
     * Test the file function
     *
     * @tests
     */
    public function it_gives_the_correct_file()
    {
        $testfile = new TestFile();
        $random = \md5(\mt_rand());
        $result = $testfile->file();
        $this->assertInternalType('string', $result);
        $this->assertEquals('', $result);

        $result = $testfile->file($random);
        $this->assertEquals($random, $result);
        $this->assertEquals($random, $testfile->file());
    }

    /**
     * Test the name function without a parameter
     *
     * @tests
     */
    public function it_gives_an_empty_name_if_not_set()
    {
        $testfile = new TestFile();
        $result = $testfile->name();
        $this->assertInternalType('string', $result);
        $this->assertEquals('', $result);
    }

    /**
     * Test the name function
     *
     * @tests
     */
    public function it_gives_the_correct_name()
    {
        $testfile = new TestFile();
        $random = \md5(\mt_rand());
        $result = $testfile->name();
        $this->assertInternalType('string', $result);
        $this->assertEquals('', $result);

        $result = $testfile->name($random);
        $this->assertEquals($random, $result);
        $this->assertEquals($random, $testfile->name());
    }

    /**
     * Test that we get an empty set if we didn't add any testcase
     *
     * @tests
     */
    public function it_returns_an_empty_set_if_there_isnt_a_testcase()
    {
        $testfile = new TestFile();
        $result = $testfile->getTestCases();
        $this->assertInternalType('array', $result);
        $this->assertEmpty($result);
    }

    /**
     * Test that we get an empty set if we didn't add any testcase
     *
     * @tests
     */
    public function it_returns_a_set_of_testcases()
    {
        $testCase = new \TJVB\Testreportmixer\Models\TestCase();
        $testfile = new TestFile();
        $testfile->addTestCase($testCase);
        $result = $testfile->getTestCases();
        $this->assertInternalType('array', $result);
        $this->assertContains($testCase, $result);
        $this->assertCount(1, $result);

        $testCase2 = new \TJVB\Testreportmixer\Models\TestCase();
        $testfile->addTestCase($testCase2);
        $result = $testfile->getTestCases();
        $this->assertInternalType('array', $result);
        $this->assertContains($testCase, $result);
        $this->assertContains($testCase2, $result);
        $this->assertCount(2, $result);
    }
}