<?php

namespace TJVB\Testreportmixer\Tests\Factories;

use TJVB\Testreportmixer\Tests\TestCase;
use TJVB\Testreportmixer\Factories\ParserFactory;
use TJVB\Testreportmixer\Parsers\ParserInterface;
use TJVB\Testreportmixer\Exceptions\FactoryException;

/**
 * Test the ParserFactory
 *
 * @author Tobias van Beek <t.vanbeek@tjvb.nl>
 *
 * @group factories
 */
class ParserFactoryTest extends TestCase
{
    /**
     * Test if we can make a parser
     *
     * @tests
     */
    public function it_can_make_a_parser()
    {
        $factory = new ParserFactory();
        $result = $factory->make('junit');
        $this->assertInstanceOf(ParserInterface::class, $result);
    }

    /**
     * Test if we can make a parser
     *
     * @tests
     */
    public function it_can_make_a_not_default_parser()
    {
        $factory = new ParserFactory();
        $result = $factory->make('\TJVB\Testreportmixer\Tests\Stubs\ParserStub');
        $this->assertInstanceOf(ParserInterface::class, $result);
    }

    /**
     * Test if we can make a parser
     *
     * @tests
     */
    public function it_can_not_make_a_non_existing_parser()
    {
        $this->expectException(FactoryException::class);
        $factory = new ParserFactory();
        $result = $factory->make('IDontExist');
        $this->assertInstanceOf(ParserInterface::class, $result);
    }
}