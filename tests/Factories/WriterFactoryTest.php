<?php

namespace TJVB\Testreportmixer\Tests\Factories;

use TJVB\Testreportmixer\Tests\TestCase;
use TJVB\Testreportmixer\Factories\WriterFactory;
use TJVB\Testreportmixer\Writers\WriterInterface;
use TJVB\Testreportmixer\Exceptions\FactoryException;

/**
 * Test the WriterFactory
 *
 * @author Tobias van Beek <t.vanbeek@tjvb.nl>
 *
 * @group factories
 */
class WriterFactoryTest extends TestCase
{
    /**
     * Test if we can make a parser
     *
     * @tests
     */
    public function it_can_make_a_parser()
    {
        $factory = new WriterFactory();
        $result = $factory->make('junit');
        $this->assertInstanceOf(WriterInterface::class, $result);
    }

    /**
     * Test if we can make a parser
     *
     * @tests
     */
    public function it_can_make_a_not_default_parser()
    {
        $factory = new WriterFactory();
        $result = $factory->make('\TJVB\Testreportmixer\Tests\Stubs\WriterStub');
        $this->assertInstanceOf(WriterInterface::class, $result);
    }

    /**
     * Test if we can make a parser
     *
     * @tests
     */
    public function it_can_not_make_a_non_existing_parser()
    {
        $this->expectException(FactoryException::class);
        $factory = new WriterFactory();
        $result = $factory->make('IDontExist');
        $this->assertInstanceOf(WriterInterface::class, $result);
    }
}