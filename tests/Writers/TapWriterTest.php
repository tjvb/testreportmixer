<?php

namespace TJVB\Testreportmixer\Tests\Writers;

use TJVB\Testreportmixer\Tests\TestCase;
use TJVB\Testreportmixer\Writers\TapWriter;
use TJVB\Testreportmixer\Writers\WriterInterface;
use TJVB\Testreportmixer\Exceptions\WriteException;
use TJVB\Testreportmixer\Exceptions\FileAccessException;
use org\bovigo\vfs\vfsStream;
use TJVB\Testreportmixer\Models\TestFile;

/**
 * Test the tap writer
 *
 * @author Tobias van Beek <t.vanbeek@tjvb.nl>
 *
 * @group writers
 */
class TapWriterWriterTest extends TestCase
{
    /**
     * Test if we can create a tap writer
     *
     * @tests
     */
    public function it_can_create_the_tap_parser()
    {
        $writer = new TapWriter();
        $this->assertInstanceOf(TapWriter::class, $writer);
        $this->assertInstanceOf(WriterInterface::class, $writer);
    }

    /**
     * Test if we get an exception if we try to write to a file without having any testdata added
     *
     * @tests
     */
    public function it_throws_an_exception_while_writing_without_data()
    {
        $writer = new TapWriter();
        $this->expectException(WriteException::class);
        $writer->writeToFile(\tempnam(sys_get_temp_dir(), 'test'));
    }

    /**
     * Test if we get an exception while we try to write to a dir what is a file
     *
     * @tests
     */
    public function it_throws_an_exception_if_write_to_dir_receivers_file()
    {
        $writer = new TapWriter();
        $writer->addTestFiles($this->getTestFiles());
        $this->expectException(FileAccessException::class);
        $writer->writeToDir(\tempnam(sys_get_temp_dir(), 'test'));
    }

    /**
     * Test if we get an exception while we try to write to a file what is a dir
     *
     * @tests
     */
    public function it_throws_an_exception_if_write_to_file_receivers_dir()
    {
        $writer = new TapWriter();
        $writer->addTestFiles($this->getTestFiles());
        $this->expectException(FileAccessException::class);
        $writer->writeToFile(sys_get_temp_dir());
    }

    /**
     * Test if we get an exception while we try to write to a dir where we can't write
     *
     * @tests
     */
    public function it_throws_an_exception_if_it_cant_write_in_directory()
    {
        $root = vfsStream::setup('testdir', 0000)->chown(vfsStream::OWNER_ROOT);
        $directory = vfsStream::newDirectory('demo', 0000)->chown(vfsStream::OWNER_ROOT)->at($root);
        $writer = new TapWriter();
        $writer->addTestFiles($this->getTestFiles());
        $this->expectException(FileAccessException::class);
        $writer->writeToDir(vfsStream::url('testdir/demo'));
    }

    /**
     * Test if we get an exception while we try to write to a dir that we can't create
     *
     * @tests
     */
    public function it_throws_an_exception_if_it_cant_create_the_directory()
    {
        $root = vfsStream::setup('testdir', 0000)->chown(vfsStream::OWNER_ROOT);
        $writer = new TapWriter();
        $writer->addTestFiles($this->getTestFiles());
        $this->expectException(FileAccessException::class);
        $writer->writeToDir(vfsStream::url('testdir/demo'));
    }

    /**
     * Test if we get an exception while we try to write to a dir that we can't create
     *
     * @tests
     */
    public function it_throws_an_exception_if_it_cant_write_the_file()
    {
        $root = vfsStream::setup('testdir', 0000)->chown(vfsStream::OWNER_ROOT);
        $file = vfsStream::newFile('demo', 0000)->chown(vfsStream::OWNER_ROOT)->at($root);
        $writer = new TapWriter();
        $writer->addTestFiles($this->getTestFiles());
        $this->expectException(FileAccessException::class);
        $writer->writeToFile(vfsStream::url('testdir/demo'));
    }

    /**
     * Test if we get an exception while we try to write to a dir that we can't create
     *
     * @tests
     */
    public function it_throws_an_exception_if_it_cant_create_the_file()
    {
        $root = vfsStream::setup('testdir', 0000)->chown(vfsStream::OWNER_ROOT);
        $writer = new TapWriter();
        $writer->addTestFiles($this->getTestFiles());
        $this->expectException(FileAccessException::class);
        $writer->writeToFile(vfsStream::url('testdir/demo'));
    }

    /**
     * Test that we can write to a file
     *
     * @tests
     */
    public function it_will_write_to_a_file()
    {
        $file = $this->tempDir . 'tap_file.tap';
        @\unlink($file);
        $writer = new TapWriter();
        $writer->addTestFiles($this->getTestFiles());
        $writer->writeToFile($file);
        $this->assertFileExists($file);
        $content = \file_get_contents($file);

        $this->assertNotEmpty($content, \print_r($content,true));
        $this->assertContains('TAP', $content);
    }

    /**
     * Test that we can write to a dir
     *
     * @tests
     */
    public function it_will_write_to_a_dir()
    {
        $dir = $this->tempDir;
        $file = $this->tempDir . 'tap.log';
        @\unlink($file);
        $writer = new TapWriter();
        $writer->addTestFiles($this->getTestFiles());
        $writer->writeToDir($dir);
        $this->assertFileExists($file);
        $content = \file_get_contents($file);

        $this->assertNotEmpty($content, \print_r($content,true));
        $this->assertContains('TAP', $content);
    }

    /**
     * Test that it will handle a successfull item
     *
     * @tests
     */
    public function it_will_handle_a_successfull_test()
    {
        $file = $this->tempDir . 'tap-succesfull.tap';
        @\unlink($file);
        // create a skipped testcase
        $testCase = $this->getTestCase();
        $testCase->status('successfull');
        $testFile = $this->getTestFiles($testCase);

        $writer = new TapWriter();
        $writer->addTestFiles($testFile);
        $writer->writeToFile($file);

        $this->assertFileExists($file);
        $content = \file_get_contents($file);

        $this->assertContains( PHP_EOL . 'ok 1 - ', $content);
    }

    /**
     * Test that it will handle a successfull item
     *
     * @tests
     */
    public function it_will_handle_a_failed_test()
    {
        $file = $this->tempDir . 'tap-failed.tap';
        @\unlink($file);
        // create a skipped testcase
        $testCase = $this->getTestCase();
        $testCase->status('failed');
        $testFile = $this->getTestFiles($testCase);

        $writer = new TapWriter();
        $writer->addTestFiles($testFile);
        $writer->writeToFile($file);

        $this->assertFileExists($file);
        $content = \file_get_contents($file);

        $this->assertContains(PHP_EOL . 'not ok 1 - ', $content);
        $this->assertContains( PHP_EOL . '  severity: fail', $content);
    }

    /**
     * Test that it will handle a successfull item
     *
     * @tests
     */
    public function it_will_handle_a_test_message()
    {
        $file = $this->tempDir . 'tap-message.tap';
        @\unlink($file);
        // create a skipped testcase
        $testCase = $this->getTestCase();
        $testCase->message('demo message');
        $testFile = $this->getTestFiles($testCase);

        $writer = new TapWriter();
        $writer->addTestFiles($testFile);
        $writer->writeToFile($file);

        $this->assertFileExists($file);
        $content = \file_get_contents($file);

        $this->assertContains(PHP_EOL . '  message: demo message', $content);
    }

    /**
     * Test that it will handle a skipped item
     *
     * @tests
     */
    public function it_will_handle_a_skip()
    {
        $file = $this->tempDir . 'tap-skip.tap';
        @\unlink($file);
        // create a skipped testcase
        $testCase = $this->getTestCase();
        $testCase->status('skipped');
        $testFile = $this->getTestFiles($testCase);

        $writer = new TapWriter();
        $writer->addTestFiles($testFile);
        $writer->writeToFile($file);

        $this->assertFileExists($file);
        $content = \file_get_contents($file);

        $this->assertContains(' - # SKIP' . PHP_EOL, $content);
    }


    /**
     * Test that it will handle a skipped item
     *
     * @tests
     */
    public function it_will_handle_an_error()
    {
        $file = $this->tempDir . 'tap-error.tap';
        @\unlink($file);
        // create a skipped testcase
        $testCase = $this->getTestCase();
        $testCase->status('error');
        $testFile = $this->getTestFiles($testCase);

        $writer = new TapWriter();
        $writer->addTestFiles($testFile);
        $writer->writeToFile($file);

        $this->assertFileExists($file);
        $content = \file_get_contents($file);

        $this->assertContains(PHP_EOL . 'not ok 1 - ', $content);
    }
    /**
     * Get a testcase model
     *
     * @return \TJVB\Testreportmixer\Models\TestCase
     */
    protected function getTestCase()
    {
        $testCase = new \TJVB\Testreportmixer\Models\TestCase();
        $testCase->assertions(0);
        $testCase->class('Demo\Class\Classname');
        $testCase->classname('Demo.Class.Classname');
        $testCase->name('it_is_a_demo');
        $testCase->assertions(1);
        $testCase->duration(0.2);
        $testCase->line(12);
        $testCase->status('successfull');
        return $testCase;
    }

    /**
     * Get an testfile array
     *
     * @param \TJVB\Testreportmixer\Models\TestCase $testcase
     * @return \TJVB\Testreportmixer\Models\TestFile[]
     */
    protected function getTestFiles(\TJVB\Testreportmixer\Models\TestCase $testcase = null)
    {
        $testFile = new TestFile();
        $testFile->name('Demo\Class\Classname');
        $testFile->file('/var/project/Demo/Class/Classname.php');
        if (\is_null($testcase)) {
            $testcase = $this->getTestCase();
        }
        $testFile->addTestCase($testcase);
        return [$testFile->file() => $testFile];
    }
}