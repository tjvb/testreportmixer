<?php

namespace TJVB\Testreportmixer\Tests\Writers;

use TJVB\Testreportmixer\Tests\TestCase;
use TJVB\Testreportmixer\Writers\JunitWriter;
use TJVB\Testreportmixer\Writers\WriterInterface;
use TJVB\Testreportmixer\Exceptions\WriteException;
use TJVB\Testreportmixer\Exceptions\FileAccessException;
use org\bovigo\vfs\vfsStream;
use TJVB\Testreportmixer\Models\TestFile;

/**
 * Test the junit writer
 *
 * @author Tobias van Beek <t.vanbeek@tjvb.nl>
 */
class JunitWriterWriterTest extends TestCase
{
    /**
     * Test if we can create a junit writer
     *
     * @tests
     */
    public function it_can_create_the_junit_parser()
    {
        $writer = new JunitWriter();
        $this->assertInstanceOf(JunitWriter::class, $writer);
        $this->assertInstanceOf(WriterInterface::class, $writer);
    }

    /**
     * Test if we get an exception if we try to write to a file without having any testdata added
     *
     * @tests
     */
    public function it_throws_an_exception_while_writing_without_data()
    {
        $writer = new JunitWriter();
        $this->expectException(WriteException::class);
        $writer->writeToFile(\tempnam(sys_get_temp_dir(), 'test'));
    }

    /**
     * Test if we get an exception while we try to write to a dir what is a file
     *
     * @tests
     */
    public function it_throws_an_exception_if_write_to_dir_receivers_file()
    {
        $writer = new JunitWriter();
        $writer->addTestFiles($this->getTestFiles());
        $this->expectException(FileAccessException::class);
        $writer->writeToDir(\tempnam(sys_get_temp_dir(), 'test'));
    }

    /**
     * Test if we get an exception while we try to write to a file what is a dir
     *
     * @tests
     */
    public function it_throws_an_exception_if_write_to_file_receivers_dir()
    {
        $writer = new JunitWriter();
        $writer->addTestFiles($this->getTestFiles());
        $this->expectException(FileAccessException::class);
        $writer->writeToFile(sys_get_temp_dir());
    }

    /**
     * Test if we get an exception while we try to write to a dir where we can't write
     *
     * @tests
     */
    public function it_throws_an_exception_if_it_cant_write_in_directory()
    {
        $root = vfsStream::setup('testdir', 0000)->chown(vfsStream::OWNER_ROOT);
        $directory = vfsStream::newDirectory('demo', 0000)->chown(vfsStream::OWNER_ROOT)->at($root);
        $writer = new JunitWriter();
        $writer->addTestFiles($this->getTestFiles());
        $this->expectException(FileAccessException::class);
        $writer->writeToDir(vfsStream::url('testdir/demo'));
    }

    /**
     * Test if we get an exception while we try to write to a dir that we can't create
     *
     * @tests
     */
    public function it_throws_an_exception_if_it_cant_create_the_directory()
    {
        $root = vfsStream::setup('testdir', 0000)->chown(vfsStream::OWNER_ROOT);
        $writer = new JunitWriter();
        $writer->addTestFiles($this->getTestFiles());
        $this->expectException(FileAccessException::class);
        $writer->writeToDir(vfsStream::url('testdir/demo'));
    }

    /**
     * Test if we get an exception while we try to write to a dir that we can't create
     *
     * @tests
     */
    public function it_throws_an_exception_if_it_cant_write_the_file()
    {
        $root = vfsStream::setup('testdir', 0000)->chown(vfsStream::OWNER_ROOT);
        $file = vfsStream::newFile('demo', 0000)->chown(vfsStream::OWNER_ROOT)->at($root);
        $writer = new JunitWriter();
        $writer->addTestFiles($this->getTestFiles());
        $this->expectException(FileAccessException::class);
        $writer->writeToFile(vfsStream::url('testdir/demo'));
    }

    /**
     * Test if we get an exception while we try to write to a dir that we can't create
     *
     * @tests
     */
    public function it_throws_an_exception_if_it_cant_create_the_file()
    {
        $root = vfsStream::setup('testdir', 0000)->chown(vfsStream::OWNER_ROOT);
        $writer = new JunitWriter();
        $writer->addTestFiles($this->getTestFiles());
        $this->expectException(FileAccessException::class);
        $writer->writeToFile(vfsStream::url('testdir/demo'));
    }

    /**
     * Test that we can write to a file
     *
     * @tests
     */
    public function it_will_write_to_a_file()
    {
        $file = $this->tempDir . 'junit_file.xml';
        @\unlink($file);
        $writer = new JunitWriter();
        $writer->addTestFiles($this->getTestFiles());
        $writer->writeToFile($file);
        $content = \file_get_contents($file);
        $this->assertFileExists($file);

        $this->assertNotEmpty($content, \print_r($content,true));
        $this->assertXmlIsValid($content);
    }

    /**
     * Test that we can write to a dir
     *
     * @tests
     */
    public function it_will_write_to_a_dir()
    {
        $dir = $this->tempDir;
        $file = $this->tempDir . 'junit.xml';
        @\unlink($file);
        $writer = new JunitWriter();
        $writer->addTestFiles($this->getTestFiles());
        $writer->writeToDir($dir);
        $content = \file_get_contents($file);
        $this->assertFileExists($file);

        $this->assertNotEmpty($content, \print_r($content,true));
        $this->assertXmlIsValid($content);
    }

    /**
     * Test that it will handle a successfull item
     *
     * @tests
     */
    public function it_will_handle_a_successfull_test()
    {
        $file = $this->tempDir . 'junit-succesfull.xml';
        @\unlink($file);
        // create a skipped testcase
        $testCase = $this->getTestCase();
        $testCase->status('successfull');
        $testFile = $this->getTestFiles($testCase);

        $writer = new JunitWriter();
        $writer->addTestFiles($testFile);
        $writer->writeToFile($file);

        $this->assertFileExists($file);
        $content = \file_get_contents($file);
        $this->assertXmlIsValid($content);

        $this->assertNotContains( '<skipped', $content);
        $this->assertNotContains( '<error', $content);
        $this->assertNotContains( '<failure', $content);
    }

    /**
     * Test that it will handle a successfull item
     *
     * @tests
     */
    public function it_will_handle_a_failed_test()
    {
        $file = $this->tempDir . 'junit-failed.xml';
        @\unlink($file);
        // create a skipped testcase
        $testCase = $this->getTestCase();
        $testCase->status('failed');
        $testFile = $this->getTestFiles($testCase);

        $writer = new JunitWriter();
        $writer->addTestFiles($testFile);
        $writer->writeToFile($file);

        $this->assertFileExists($file);
        $content = \file_get_contents($file);

        $this->assertNotContains( '<skipped', $content);
        $this->assertNotContains( '<error', $content);
        $this->assertContains( '<failure', $content);
    }

    /**
     * Test that it will handle a successfull item
     *
     * @tests
     */
    public function it_will_handle_a_test_message()
    {
        $file = $this->tempDir . 'junit-message.xml';
        @\unlink($file);
        // create a skipped testcase
        $testCase = $this->getTestCase();
        $testCase->status('failed');
        $testCase->message('demo message');
        $testFile = $this->getTestFiles($testCase);

        $writer = new JunitWriter();
        $writer->addTestFiles($testFile);
        $writer->writeToFile($file);

        $this->assertFileExists($file);
        $content = \file_get_contents($file);

        $this->assertNotContains( '<skipped', $content);
        $this->assertNotContains( '<error', $content);
        $this->assertContains( '<failure>demo message</failure', $content);
    }

    /**
     * Test that it will handle a skipped item
     *
     * @tests
     */
    public function it_will_handle_a_skip()
    {
        $file = $this->tempDir . 'junit-skip.xml';
        @\unlink($file);
        // create a skipped testcase
        $testCase = $this->getTestCase();
        $testCase->status('skipped');
        $testFile = $this->getTestFiles($testCase);

        $writer = new JunitWriter();
        $writer->addTestFiles($testFile);
        $writer->writeToFile($file);

        $this->assertFileExists($file);
        $content = \file_get_contents($file);

        $this->assertContains( '<skipped', $content);
        $this->assertNotContains( '<error', $content);
        $this->assertNotContains( '<failure', $content);
    }

    /**
     * Test that it will handle a skipped item
     *
     * @tests
     */
    public function it_will_handle_an_error()
    {
        $file = $this->tempDir . 'junit-error.xml';
        @\unlink($file);
        // create a skipped testcase
        $testCase = $this->getTestCase();
        $testCase->status('error');
        $testFile = $this->getTestFiles($testCase);

        $writer = new JunitWriter();
        $writer->addTestFiles($testFile);
        $writer->writeToFile($file);

        $this->assertFileExists($file);
        $content = \file_get_contents($file);

        $this->assertNotContains( '<skipped', $content);
        $this->assertContains( '<error', $content);
        $this->assertNotContains( '<failure', $content);
    }

    /**
     * Get a testcase model
     *
     * @return \TJVB\Testreportmixer\Models\TestCase
     */
    protected function getTestCase()
    {
        $testCase = new \TJVB\Testreportmixer\Models\TestCase();
        $testCase->assertions(0);
        $testCase->class('Demo\Class\Classname');
        $testCase->classname('Demo.Class.Classname');
        $testCase->name('it_is_a_demo');
        $testCase->assertions(1);
        $testCase->duration(0.2);
        $testCase->line(12);
        $testCase->status('successfull');
        return $testCase;
    }

    /**
     * Get an testfile array
     *
     * @param \TJVB\Testreportmixer\Models\TestCase $testcase
     * @return \TJVB\Testreportmixer\Models\TestFile[]
     */
    protected function getTestFiles(\TJVB\Testreportmixer\Models\TestCase $testcase = null)
    {
        $testFile = new TestFile();
        $testFile->name('Demo\Class\Classname');
        $testFile->file('/var/project/Demo/Class/Classname.php');
        if (\is_null($testcase)) {
            $testcase = $this->getTestCase();
        }
        $testFile->addTestCase($testcase);
        return [$testFile->file() => $testFile];
    }
}