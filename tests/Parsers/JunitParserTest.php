<?php

namespace TJVB\Testreportmixer\Tests\Parsers;

use TJVB\Testreportmixer\Tests\TestCase;
use TJVB\Testreportmixer\Parsers\JunitParser;
use TJVB\Testreportmixer\Parsers\ParserInterface;
use TJVB\Testreportmixer\Exceptions\FileAccessException;
use TJVB\Testreportmixer\Exceptions\Parse\XmlException;
use org\bovigo\vfs\vfsStream;

/**
 * The test for the Junit Parser
 *
 * @author Tobias van Beek <t.vanbeek@tjvb.nl>
 *
 * @group parsers
 */
class JunitParserTest extends TestCase
{
    /**
     * Test if we can create a junit parser
     *
     * @tests
     */
    public function it_can_create_the_junit_parser()
    {
        $parser = new JunitParser();
        $this->assertInstanceOf(JunitParser::class, $parser);
        $this->assertInstanceOf(ParserInterface::class, $parser);
    }

    /**
     * Test that we get an exception if the file doesn't exist
     *
     * @tests
     */
    public function it_will_throw_an_exception_if_file_doesnt_exist()
    {
        $this->expectException(FileAccessException::class);
        $parser = new JunitParser();
        $parser->parseFile('idontexist');
        $this->assertFalse(true);
    }

    /**
     * Test that we get an exception if the file isn't readable
     *
     * @tests
     */
    public function it_will_throw_an_exception_if_file_isnt_readable()
    {
        $this->expectException(FileAccessException::class);

        $root = vfsStream::setup('testdir', 0000);
        vfsStream::newFile('demo', 0000)->chown(vfsStream::OWNER_ROOT)->at($root);
        $parser = new JunitParser();
        $parser->parseFile(vfsStream::url('testdir/demo'));
    }

    /**
     * Test that we get an exception if the file isn't readable
     *
     * @tests
     */
    public function it_will_throw_an_exception_if_file_is_a_dir()
    {
        $this->expectException(FileAccessException::class);

        $parser = new JunitParser();
        $parser->parseFile(sys_get_temp_dir());

        $this->markTestIncomplete();
    }

    /**
     * Test that we get an exception if the xml isn'r readable
     *
     * @test
     */
    public function it_will_throw_an_exception_if_xml_is_invalid()
    {
        $this->expectException(XmlException::class);
        $tempFile = \tempnam(sys_get_temp_dir(), 'test');
        \file_put_contents($tempFile, 'no xml so invalid');
        $parser = new JunitParser();
        $parser->parseFile($tempFile);
        @\unlink($tempFile);
    }

    /**
     * Test the parsing of xml
     *
     * @tests
     */
    public function it_will_parse_the_xml()
    {
        $parser = new JunitParser();
        $result = $parser->parseFile($this->dataDir . 'junit.xml');
        $this->assertTrue($result);
    }

    /**
     * Test if we get a number from the assertions call
     *
     * @tests
     */
    public function it_will_give_an_assertions_number()
    {
        // parse a file with a known number of assertions
        $parser = new JunitParser();
        $parser->parseFile($this->dataDir . 'junit.xml');
        $this->assertEquals(26, $parser->assertions());
    }

    /**
     * Test if we get a number from the assertions call
     *
     * @tests
     */
    public function it_will_give_an_assertions_number_without_parsing()
    {
        $parser = new JunitParser();
        $this->assertEquals(0, $parser->assertions());
    }

    /**
     * Test if we get a number from the errors call
     *
     * @tests
     */
    public function it_will_give_an_errors_number()
    {
        // parse a file with a known number of errors
        $parser = new JunitParser();
        $parser->parseFile($this->dataDir . 'junit.xml');
        $this->assertEquals(1, $parser->errors());
    }

    /**
     * Test if we get a number from the errors call
     *
     * @tests
     */
    public function it_will_give_an_errors_number_without_parsing()
    {
        $parser = new JunitParser();
        $this->assertEquals(0, $parser->errors());
    }

    /**
     * Test if we get a number from the failures call
     *
     * @tests
     */
    public function it_will_give_an_failures_number()
    {
        // parse a file with a known number of failures
        $parser = new JunitParser();
        $parser->parseFile($this->dataDir . 'junit.xml');
        $this->assertEquals(1, $parser->failures());
    }

    /**
     * Test if we get a number from the failures call
     *
     * @tests
     */
    public function it_will_give_an_failures_number_without_parsing()
    {
        $parser = new JunitParser();
        $this->assertEquals(0, $parser->failures());
    }

    /**
     * Test if we get a number from the skipped call
     *
     * @tests
     */
    public function it_will_give_an_skipped_number()
    {
        // parse a file with a known number of skipped
        $parser = new JunitParser();
        $parser->parseFile($this->dataDir . 'junit.xml');
        $this->assertEquals(6, $parser->skipped());
    }

    /**
     * Test if we get a number from the skipped call
     *
     * @tests
     */
    public function it_will_give_an_skipped_number_without_parsing()
    {
        $parser = new JunitParser();
        $this->assertEquals(0, $parser->skipped());
    }

    /**
     * Test if we get a number from the tests call
     *
     * @tests
     */
    public function it_will_give_an_tests_number()
    {
        // parse a file with a known number of tests
        $parser = new JunitParser();
        $parser->parseFile($this->dataDir . 'junit.xml');
        $this->assertEquals(24, $parser->tests());
    }

    /**
     * Test if we get a number from the tests call
     *
     * @tests
     */
    public function it_will_give_an_tests_number_without_parsing()
    {
        $parser = new JunitParser();
        $this->assertEquals(0, $parser->tests());
    }

    /**
     * Test if we get an empty set back
     *
     * @tests
     */
    public function it_will_return_empty_array_for_testfiles_if_not_parsed()
    {
        $parser = new JunitParser();
        $this->assertEmpty($parser->getTestFiles());
    }

    /**
     * Test if we get an empty set back
     *
     * @tests
     */
    public function it_will_return_testfiles_if_parsed()
    {
        $parser = new JunitParser();
        $parser->parseFile($this->dataDir . 'junit.xml');
        $testFiles = $parser->getTestFiles();
        $this->assertNotEmpty($testFiles);
        foreach ($testFiles as $file) {
//             $this->assertInstanceOf(TestFileInterface::class, $file);
        }
    }

    /**
     * Test the parsing of xml with a testcase without attributes
     *
     * @tests
     */
    public function it_will_parse_if_testcase_didnt_have_attributes()
    {
        $parser = new JunitParser();
        $result = $parser->parseFile($this->dataDir . 'junit_no_attributes.xml');
        $this->assertTrue($result);
    }
}