# Default merge request

(Explain what this merge does)  
(Optional mention the issue that is connected to this merge request)

## Check the requirements

* [ ] The code confirms to PSR-2
* [ ] There are tests added and they work with vendor/bin/phpunit
* [ ] The changes are documented in CHANGELOG.md and if needed in README.md
* [ ] The changes doesn't conflict with SemVer
* [ ] There is only one functional change in this merge request

## Task before creating the merge request
* [ ] Add the labels that match this merge request
* [ ] Check the commit(s) below that everything is correct and nothing is missing
