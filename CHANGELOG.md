# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).


## [Unreleased]




## 1.0.3
- Fix the parsing of junit xml files where not all the attributes are set on a testcase

## 1.0.2
- Add the missing bin part in the composer.json

## 1.0.1

- Changed the symfony/console requirment from ^3.3 to ^3.0

## 1.0.0

### Added
- Add junit parser
- Add junit writer
- Add tap writer
- Add a readme
- Add a changelog
- Add a code of conduct
- Add a contributing
