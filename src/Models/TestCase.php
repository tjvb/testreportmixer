<?php

namespace TJVB\Testreportmixer\Models;

use TJVB\Testreportmixer\Models\Interfaces\TestCaseInterface;

/**
 * The testcase where we place the test information
 *
 * @author Tobias van Beek <t.vanbeek@tjvb.nl>
 */
class TestCase implements TestCaseInterface
{
    /**
     * The class
     *
     * @var string
     */
    protected $class = '';

    /**
     * The classname
     *
     * @var string
     */
    protected $classname = '';

    /**
     * The name
     *
     * @var string
     */
    protected $name = '';

    /**
     * The duration
     *
     * @var float
     */
    protected $duration = 0.0;

    /**
     * The number of assertions
     *
     * @var int
     */
    protected $assertions = 0;

    /**
     * The line of the testcase
     *
     * @var int
     */
    protected $line = 0;

    /**
     * The status of this case
     *
     * @var string
     */
    protected $status  = '';

    /**
     * The message of this case
     *
     * @var string
     */
    protected $message = '';

    /**
     * Set or get the class
     *
     * @param string $class
     *
     * @return string
     */
    public function class(string $class = null): string
    {
        if ($class !== null) {
            $this->class = $class;
        }
        return $this->class;
    }

    /**
     * Set or get the class name
     *
     * @param string $classname
     *
     * @return string
     */
    public function classname(string $classname = null) : string
    {
        if ($classname !== null) {
            $this->classname = $classname;
        }
        return $this->classname;
    }

    /**
     * Set or get the name
     *
     * @param string $name
     *
     * @return string
     */
    public function name(string $name = null) : string
    {
        if ($name !== null) {
            $this->name = $name;
        }
        return $this->name;
    }

    /**
     * Set or get the number of assertions
     *
     * @param int $assertions
     *
     * @return int
     */
    public function assertions(int $assertions = null) : int
    {
        if ($assertions!== null) {
            $this->assertions = $assertions;
        }
        return $this->assertions;
    }

    /**
     * Set or get the line
     *
     * @param int line
     *
     * @return int
     */
    public function line(int $line = null) : int
    {
        if ($line!== null) {
            $this->line= $line;
        }
        return $this->line;
    }

    /**
     * Set or get the duration
     *
     * @param float $duration
     *
     * @return float
     */
    public function duration(float $duration = null): float
    {
        if ($duration!== null) {
            $this->duration = $duration;
        }
        return $this->duration;
    }

    /**
     * Set or get the status
     *
     * @param string $status
     *
     * @return string
     */
    public function status(string $status = null): string
    {
        if ($status!== null) {
            $this->status = $status;
        }
        return $this->status;
    }

    /**
     * Set or get the message
     *
     * @param string $message
     *
     * @return string
     */
    public function message(string $message = null): string
    {
        if ($message !== null) {
            $this->message= $message;
        }
        return $this->message;
    }
}
