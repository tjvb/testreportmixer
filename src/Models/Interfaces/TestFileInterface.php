<?php

namespace TJVB\Testreportmixer\Models\Interfaces;

/**
 * The interface for the testfiles
 *
 * @author Tobias van Beek <t.vanbeek@tjvb.nl>
 */
interface TestFileInterface
{
    /**
     * Set or get the name
     *
     * @param string $name
     *
     * @return string
     */
    public function name(string $name = null) : string;

    /**
     * Add a testcase to the file
     *
     * @param TestCaseInterface $testcase
     */
    public function addTestCase(TestCaseInterface $testcase);

    /**
     * Get the testcases
     *
     * @return array
     */
    public function getTestCases() : array;
}
