<?php

namespace TJVB\Testreportmixer\Models\Interfaces;

/**
 * The interface for the TestCase
 *
 * @author Tobias van Beek <t.vanbeek@tjvb.nl>
 */
interface TestCaseInterface
{
    /**
     * Set or get the class
     *
     * @param string $class
     *
     * @return string
     */
    public function class(string $class = null) : string;

    /**
     * Set or get the class name
     *
     * @param string $classname
     *
     * @return string
     */
    public function classname(string $classname = null) : string;

    /**
     * Set or get the name
     *
     * @param string $name
     *
     * @return string
     */
    public function name(string $name = null) : string;

    /**
     * Set or get the number of assertions
     *
     * @param int $assertions
     *
     * @return int
     */
    public function assertions(int $assertions = null) : int;

    /**
     * Set or get the line
     *
     * @param int line
     *
     * @return int
     */
    public function line(int $line = null) : int;

    /**
     * Set or get the duration
     *
     * @param float $duration
     *
     * @return float
     */
    public function duration(float $duration = null): float;

    /**
     * Set or get the message
     *
     * @param string $message
     *
     * @return string
     */
    public function message(string $message = null): string;

    /**
     * Set or get the status
     *
     * @param string $status
     *
     * @return string
     */
    public function status(string $status = null): string;
}
