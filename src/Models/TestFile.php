<?php

namespace TJVB\Testreportmixer\Models;

use TJVB\Testreportmixer\Models\Interfaces\TestFileInterface;
use TJVB\Testreportmixer\Models\Interfaces\TestCaseInterface;

/**
 * The testfile
 *
 * @author Tobias van Beek <t.vanbeek@tjvb.nl>
 */
class TestFile implements TestFileInterface
{
    /**
     * The file
     *
     * @var string
     */
    protected $file = '';

    /**
     * The name
     *
     * @var string
     */
    protected $name = '';

    /**
     * The set with the testcases
     *
     * @var array
     */
    protected $testcases = [];

    /**
     * Set or get the file
     *
     * @param string $file
     *
     * @return string
     */
    public function file(string $file = null): string
    {
        if ($file !== null) {
            $this->file = $file;
        }
        return $this->file;
    }

    /**
     * Set or get the name
     *
     * @param string $name
     *
     * @return string
     */
    public function name(string $name = null): string
    {
        if ($name !== null) {
            $this->name = $name;
        }
        return $this->name;
    }

    /**
     * Add a testcase to the file
     *
     * @param TestCaseInterface $testcase
     */
    public function addTestCase(TestCaseInterface $testcase)
    {
        $this->testcases[] = $testcase;
    }

    /**
     * Get the testcases
     *
     * @return array
     */
    public function getTestCases(): array
    {
        return $this->testcases;
    }
}
