<?php

namespace TJVB\Testreportmixer\Commands;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use TJVB\Testreportmixer\Factories\ParserFactory;
use TJVB\Testreportmixer\Exceptions\FactoryException;
use TJVB\Testreportmixer\Factories\WriterFactory;
use TJVB\Testreportmixer\Exceptions\ParseException;
use TJVB\Testreportmixer\Exceptions\WriteException;

/**
 * The Convert command from TestReportMixer
 *
 * @author Tobias van Beek <t.vanbeek@tjvb.nl>
 */
class ConvertCommand extends Command
{
    /**
     * Set the configuretion for the command
     */
    protected function configure()
    {
        $this->setName('convert')
            ->setDescription('Convert a test report to another report')
            ->setHelp('convert the test format from one or more input files to an output file')

            ->addArgument('outputformat', InputArgument::REQUIRED)
            ->addArgument('outputfile', InputArgument::REQUIRED)
            ->addArgument('inputformat', InputArgument::REQUIRED)
            ->addArgument('inputfiles', InputArgument::IS_ARRAY | InputArgument::REQUIRED)
            ;
    }

    /**
     * Execute the convert command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $parserFactory = new ParserFactory();
        try {
            $parser = $parserFactory->make($input->getArgument('inputformat'));
        } catch (FactoryException $e) {
            $output->writeln('<error>' . $e->getMessage().'</error>');
            return false;
        }
        $writerFactory = new WriterFactory();
        try {
            $writer = $writerFactory->make($input->getArgument('outputformat'));
        } catch (FactoryException $e) {
            $output->writeln('<error>' . $e->getMessage().'</error>');
            return false;
        }
        foreach ($input->getArgument('inputfiles') as $inputFile) {
            try {
                $parser->parseFile($inputFile);
            } catch (ParseException $e) {
                $output->writeln('<error>Failed to parse ' . $inputFile. PHP_EOL . $e->getMessage().'</error>');
            }
        }
        $writer->addTestFiles($parser->getTestFiles());

        try {
            $writer->writeToFile($input->getArgument('outputfile'));
        } catch (WriteException $e) {
            $output->writeln('<error>Failed to write' . PHP_EOL . $e->getMessage().'</error>');
        }
        $output->writeln('Finished');
    }
}
