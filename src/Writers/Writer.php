<?php

namespace TJVB\Testreportmixer\Writers;

use TJVB\Testreportmixer\Exceptions\FileAccessException;

/**
 *
 * @author Tobias van Beek <t.vanbeek@tjvb.nl>
 *
 */
abstract class Writer implements WriterInterface
{
    /**
     * Check the write access to a file
     *
     * @param string $filepath
     *
     * @throws FileAccessException
     *
     * @return void
     */
    protected function checkFileAccess(string $filepath)
    {
        if (\is_dir($filepath)) {
            throw FileAccessException::isDirNeedFile($filepath);
        }
        if (\file_exists($filepath) && !\is_writable($filepath)) {
            throw FileAccessException::noWriteAccess($filepath);
        }
        if (!\file_exists($filepath) && !\is_writable(\pathinfo($filepath, PATHINFO_DIRNAME))) {
            // the directory doesn't exist and we can't create it
            throw FileAccessException::noCreateAccess($filepath);
        }
    }
    /**
     * Check the write access to a directory
     *
     * @param string $filepath
     *
     * @throws FileAccessException
     *
     * @return void
     */
    protected function checkDirectoryAccess(string $filepath)
    {
        if (\is_file($filepath)) {
            throw FileAccessException::isFileNeedDir($filepath);
        }
        if (\file_exists($filepath) && !\is_writable($filepath)) {
            throw FileAccessException::noWriteAccess($filepath);
        }
        if (!\file_exists($filepath) && !\is_writable(\pathinfo($filepath, PATHINFO_DIRNAME))) {
            // the directory doesn't exist and we can't create it
            throw FileAccessException::noCreateAccess($filepath);
        }
    }
}
