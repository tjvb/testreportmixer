<?php

namespace TJVB\Testreportmixer\Writers;

use TJVB\Testreportmixer\Exceptions\WriteException;
use TJVB\Testreportmixer\Exceptions\FileAccessException;

/**
 * The Junit xml writer
 *
 * @author Tobias van Beek <t.vanbeek@tjvb.nl>
 */
class JunitWriter extends Writer
{
    /**
     * The testfiles
     *
     * @var array
     */
    protected $testFiles = [];

    /**
     * Add the testfiles to the writer
     *
     * @param array $testfiles
     */
    public function addTestFiles(array $testfiles)
    {
        $this->testFiles = \array_merge($this->testFiles, $testfiles);
    }

    /**
     * The function to write the data to a file
     *
     * @param string $path
     *
     * @throws FileAccessException
     * @throws WriteException
     *
     * @return bool
     */
    public function writeToFile(string $path): bool
    {
        if (empty($this->testFiles)) {
            throw WriteException::noData();
        }
        $this->checkFileAccess($path);
        $this->getXml()->asXML($path);

        return true;
    }

    /**
     * Get the xml in an SimpleXMLElement
     *
     * @return \SimpleXMLElement
     */
    protected function getXml() : \SimpleXMLElement
    {
        $xml = new \SimpleXMLElement('<testsuites></testsuites>');
        $testSuiteSet = $xml->addChild('testsuite');
        $totalAssertions = $totalErrors = $totalFailures = $totalSkipped = $totalTests = 0;
        $totalTime = 0.0;
        foreach ($this->testFiles as $file) {
            $fileAssertions = $fileErrors = $fileFailures = $fileSkipped = 0;
            $fileTime = 0.0;

            $testSuite = $testSuiteSet->addChild('testsuite');
            $testSuite->addAttribute('name', $file->name());
            $testSuite->addAttribute('file', $file->file());
            foreach ($file->getTestCases() as $testCase) {
                $testCaseXml = $testSuite->addChild('testcase');
                $testCaseXml->addAttribute('class', $testCase->class());
                $testCaseXml->addAttribute('classname', $testCase->classname());
                $testCaseXml->addAttribute('name', $testCase->name());
                $testCaseXml->addAttribute('line', $testCase->line());
                $testCaseXml->addAttribute('assertions', $testCase->assertions());
                $testCaseXml->addAttribute('time', $testCase->duration());
                $message = $testCase->message();
                if (empty($message)) {
                    $message = null;
                }
                switch ($testCase->status()) {
                    case 'skipped':
                        $fileSkipped++;
                        $testCaseXml->addChild('skipped', $message);
                        break;
                    case 'failed':
                        $fileFailures++;
                        $testCaseXml->addChild('failure', $message);
                        break;
                    case 'error':
                        $fileErrors++;
                        $testCaseXml->addChild('error', $message);
                        break;
                }
                $fileAssertions += $testCase->assertions();
                $fileTime += $testCase->duration();
            }
            $testSuite->addAttribute('tests', count($file->getTestCases()));
            $testSuite->addAttribute('assertions', $fileAssertions);
            $testSuite->addAttribute('errors', $fileErrors);
            $testSuite->addAttribute('failures', $fileFailures);
            $testSuite->addAttribute('skipped', $fileSkipped);
            $testSuite->addAttribute('time', $fileTime);
            $totalTests += count($file->getTestCases());
            $totalAssertions += $fileAssertions;
            $totalErrors += $fileErrors;
            $totalFailures += $fileFailures;
            $totalSkipped += $fileSkipped;
            $totalTime += $fileTime;
        }
        $testSuiteSet->addAttribute('tests', $totalTests);
        $testSuiteSet->addAttribute('assertions', $totalAssertions);
        $testSuiteSet->addAttribute('errors', $totalErrors);
        $testSuiteSet->addAttribute('failures', $totalFailures);
        $testSuiteSet->addAttribute('skipped', $totalSkipped);
        $testSuiteSet->addAttribute('time', $totalTime);
        return $xml;
    }

    /**
     * The function to write the data to a directory
     *
     * @param string $path
     *
     * @throws FileAccessException
     * @throws WriteException
     *
     * @return bool
     */
    public function writeToDir(string $path): bool
    {
        $this->checkDirectoryAccess($path);
        return $this->writeToFile($path . DIRECTORY_SEPARATOR . 'junit.xml');
    }
}
