<?php

namespace TJVB\Testreportmixer\Writers;

use TJVB\Testreportmixer\Exceptions\WriteException;
use TJVB\Testreportmixer\Models\TestCase;
use TJVB\Testreportmixer\Models\Interfaces\TestCaseInterface;
use TJVB\Testreportmixer\Exceptions\FileAccessException;

/**
 * The TAP writer, see https://testanything.org/ for information about TAP
 *
 * @author Tobias van Beek <t.vanbeek@tjvb.nl>
 */
class TapWriter extends Writer
{

    /**
     * The testfiles
     *
     * @var array
     */
    protected $testFiles = [];

    /**
     * Add the testfiles to the writer
     *
     * @param array $testfiles
     */
    public function addTestFiles(array $testfiles)
    {
        $this->testFiles = \array_merge($this->testFiles, $testfiles);
    }

    /**
     * The function to write the data to a file
     *
     * @param string $path
     *
     * @throws FileAccessException
     * @throws WriteException
     *
     * @return bool
     */
    public function writeToFile(string $path): bool
    {
        if (empty($this->testFiles)) {
            throw WriteException::noData();
        }
        $this->checkFileAccess($path);
        $fileHandler = \fopen($path, 'w');
        \fwrite($fileHandler, 'TAP version 13' . PHP_EOL);
        $counter = 0;
        foreach ($this->testFiles as $file) {
            foreach ($file->getTestCases() as $testCase) {
                $counter++;
                $line = $this->getTestCaseLine($testCase, $counter);
                \fwrite($fileHandler, $line);
            }
        }
        \fwrite($fileHandler, '1..' . $counter . PHP_EOL);
        \fclose($fileHandler);
        return true;
    }

    /**
     * Get a line for a testcase
     *
     * @param TestCaseInterface $testCase
     * @param int $counter
     *
     * @return string
     */
    protected function getTestCaseLine(TestCaseInterface $testCase, int $counter)
    {
        $line = '';
        if ($testCase->status() == 'skipped') {
            return 'ok ' . $counter . ' - # SKIP' . PHP_EOL;
        }
        if ($testCase->status() != 'successfull') {
            $line ='not ';
        }
        $line .= 'ok '.$counter.' - ' . $testCase->class().'::' . $testCase->name() . PHP_EOL;
        $message = '';
        if ($testCase->message()) {
            $message .= '  message: ' . $testCase->message();
        }
        if ($testCase->status() == 'failed') {
            $message .= '  severity: fail' . PHP_EOL;
        }
        if ($message != '') {
            $line .= '  ---' . PHP_EOL . rtrim($message) . '  ...' . PHP_EOL;
        }
        return $line;
    }

    /**
     * The function to write the data to a directory
     *
     * @param string $path
     *
     * @throws FileAccessException
     * @throws WriteException
     *
     * @return bool
     */
    public function writeToDir(string $path): bool
    {
        $this->checkDirectoryAccess($path);
        return $this->writeToFile($path . DIRECTORY_SEPARATOR . 'tap.log');
    }
}
