<?php

namespace TJVB\Testreportmixer\Writers;

use TJVB\Testreportmixer\Exceptions\WriteException;

/**
 *
 * @author Tobias van Beek <t.vanbeek@tjvb.nl>
 *
 */
interface WriterInterface
{
    /**
     * Add the testfiles to the writer
     *
     * @param array $testfiles
     */
    public function addTestFiles(array $testfiles);

    /**
     * The function to write the data to a file
     *
     * @param string $path
     *
     * @throws WriteException
     *
     * @return bool
     */
    public function writeToFile(string $path) : bool;

    /**
     * The function to write the data to a directory
     *
     * @param string $path
     *
     * @throws WriteException
     *
     * @return bool
     */
    public function writeToDir(string $path) : bool;
}
