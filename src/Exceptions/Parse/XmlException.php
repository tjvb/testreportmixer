<?php

namespace TJVB\Testreportmixer\Exceptions\Parse;

use TJVB\Testreportmixer\Exceptions\ParseException;

/**
 * The exception that we have a problem with parsing the xml
 *
 * @author Tobias van Beek <t.vanbeek@tjvb.nl>
 */
class XmlException extends ParseException
{
    /**
     * Return a XmlException with the errors in a
     *
     * @param array $errors
     *
     * @return \TJVB\Testreportmixer\Exceptions\Parse\XmlException
     */
    public static function xmlerrors(array $errors)
    {
        return new self('Failed to parse the xml' . PHP_EOL . 'errors:' . PHP_EOL . self::xmlErrorsToString($errors));
    }

    /**
     * Transform an array with xml errors to a string
     *
     * @param array $errors
     *
     * @return string
     */
    protected static function xmlErrorsToString(array $errors)
    {
        $string = '';
        foreach ($errors as $error) {
            if (!($error instanceof \LibXMLError)) {
                // it is an error but not an LibXMLError
                $string .= 'UNKNOWN ERROR: ' . \print_r($error, true) . PHP_EOL;
                continue;
            }
            switch ($error->level) {
                case LIBXML_ERR_ERROR:
                    $string .= 'ERROR: ';
                    break;
                case LIBXML_ERR_FATAL:
                    $string .= 'FATAL: ';
                    break;
                case LIBXML_ERR_WARNING:
                    $string .= 'WARNING: ';
                    break;
                default:
                    $string .= 'UNKNOWN: ';
                    break;
            }
            $string .= 'LINE ' . $error->line . ', Column: ' . $error->column . ', Code: ' . $error->code;
            if ($error->message) {
                $string .= ', Message: ' . $error->message;
            }
            $string .= PHP_EOL;
        }
        return trim($string);
    }
}
