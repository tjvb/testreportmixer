<?php

namespace TJVB\Testreportmixer\Exceptions;

/**
 *
 * @author Tobias van Beek <t.vanbeek@tjvb.nl>
 */
class FactoryException extends TestreportmixerException
{
    /**
     * We can't find something for a factory
     *
     * @param string $name
     * @param string $type
     *
     * @return \TJVB\Testreportmixer\Exceptions\FactoryException
     */
    public static function notFoundFor(string $name, string $type)
    {
        return new static('We can\'t found a ' . $type . ' for ' . $name);
    }
}
