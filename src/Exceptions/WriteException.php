<?php

namespace TJVB\Testreportmixer\Exceptions;

/**
 * The exception that writing a file failed
 *
 * @author Tobias van Beek <t.vanbeek@tjvb.nl>
 */
class WriteException extends TestreportmixerException
{
    /**
     * Write that we didn't have anything to write
     *
     * @return \TJVB\Testreportmixer\Exceptions\WriteException
     */
    public static function noData()
    {
        return new static('We didn\'t have anything to write');
    }
}
