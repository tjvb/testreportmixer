<?php

namespace TJVB\Testreportmixer\Exceptions;

/**
 * The exception that parsing a file failed
 *
 * @author Tobias van Beek <t.vanbeek@nl.nl>
 */
class ParseException extends TestreportmixerException
{

}
