<?php

namespace TJVB\Testreportmixer\Exceptions;

/**
 * The exception that there is something wrong with the file access
 *
 * @author Tobias van Beek <t.vanbeek@tjvb.nl>
 */
class FileAccessException extends TestreportmixerException
{
    /**
     * We are not able to create a file or directory
     *
     * @param string $filepath
     *
     * @return \TJVB\Testreportmixer\Exceptions\FileAccessException
     */
    public static function noCreateAccess(string $filepath)
    {
        return new static('We can\'t create the file or directory ' . $filepath);
    }

    /**
     * We are not able to read a file
     *
     * @param string $filepath
     *
     * @return \TJVB\Testreportmixer\Exceptions\FileAccessException
     */
    public static function noReadAccess(string $filepath)
    {
        return new static('We can\'t read the file ' . $filepath);
    }

    /**
     * We are not able to write a file
     *
     * @param string $filepath
     *
     * @return \TJVB\Testreportmixer\Exceptions\FileAccessException
     */
    public static function noWriteAccess(string $filepath)
    {
        return new static('We can\'t write the file ' . $filepath);
    }

    /**
     * We are not able to write or read a file because it is a directory
     *
     * @param string $filepath
     *
     * @return \TJVB\Testreportmixer\Exceptions\FileAccessException
     */
    public static function isDirNeedFile(string $filepath)
    {
        return new static('We can\'t write the file ' . $filepath . ' because it is a dir');
    }

    /**
     * We are not able to write or read a directory because it is a file
     *
     * @param string $filepath
     *
     * @return \TJVB\Testreportmixer\Exceptions\FileAccessException
     */
    public static function isFileNeedDir(string $filepath)
    {
        return new static('We can\'t write the file ' . $filepath . ' because it is a dir');
    }
}
