<?php

namespace TJVB\Testreportmixer\Exceptions;

/**
 * The base Exception for all the Testreportmixer exceptions
 *
 * @author Tobias van Beek <t.vanbeek@tjvb.nl>
 */
class TestreportmixerException extends \Exception
{

}
