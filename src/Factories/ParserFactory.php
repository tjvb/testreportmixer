<?php

namespace TJVB\Testreportmixer\Factories;

use TJVB\Testreportmixer\Parsers\ParserInterface;
use TJVB\Testreportmixer\Exceptions\FactoryException;

/**
 * The factory to make a parser
 *
 * @author Tobias van Beek <t.vanbeek@tjvb.nl>
 */
class ParserFactory
{
    /**
     * Make a parser
     *
     * @param string $name
     *
     * @throws \FactoryException
     *
     * @return ParserInterface
     */
    public function make($name)
    {
        $className = \ucfirst($name);
        $defaultParserName = 'TJVB\\Testreportmixer\\Parsers\\' . $className .'Parser';
        if (\class_exists($defaultParserName) && \is_subclass_of($defaultParserName, ParserInterface::class)) {
            // it is a buildin parser
            return new $defaultParserName();
        }
        if (\class_exists($className) && \is_subclass_of($className, ParserInterface::class)) {
            // it is another parser
            return new $className();
        }
        throw FactoryException::notFoundFor($name, 'Parser');
    }
}
