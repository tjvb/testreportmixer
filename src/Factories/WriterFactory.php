<?php

namespace TJVB\Testreportmixer\Factories;

use TJVB\Testreportmixer\Writers\WriterInterface;
use TJVB\Testreportmixer\Exceptions\FactoryException;

/**
 * The factory to make a Writer
 *
 * @author Tobias van Beek <t.vanbeek@tjvb.nl>
 */
class WriterFactory
{
    /**
     * Make a parser
     *
     * @param string $name
     *
     * @throws \FactoryException
     *
     * @return WriterInterface
     */
    public function make($name)
    {
        $className = \ucfirst($name);
        $defaultParserName = 'TJVB\\Testreportmixer\\Writers\\' . $className .'Writer';
        if (\class_exists($defaultParserName) && \is_subclass_of($defaultParserName, WriterInterface::class)) {
            // it is a buildin parser
            return new $defaultParserName();
        }
        if (\class_exists($className) && \is_subclass_of($className, WriterInterface::class)) {
            // it is another parser
            return new $className();
        }
        throw FactoryException::notFoundFor($name, 'Writer');
    }
}
