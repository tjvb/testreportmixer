<?php
namespace TJVB\Testreportmixer\Parsers;

use TJVB\Testreportmixer\Exceptions\Parse\XmlException;
use TJVB\Testreportmixer\Models\TestCase;
use TJVB\Testreportmixer\Models\TestFile;

/**
 * The parser that parse the junit xml files
 *
 * @author Tobias van Beek <t.vanbeek@tjvb.nl>
 */
class JunitParser extends Parser
{
    /**
     * The xml that we parse
     *
     * @var \SimpleXMLElement
     */
    protected $xml = null;

    /**
     * The statistics
     *
     * @var array
     */
    protected $statistics = [
        'tests' => 0,
        'assertions' => 0,
        'errors' => 0,
        'failures' => 0,
        'skipped' => 0,
        'time' => 0,
    ];

    /**
     * The files with tests
     *
     * @var array
     */
    protected $testFiles = [];

    /**
     * Parse the files
     *
     * @param string $filepath
     * @param array $options
     *
     * @throws ParseException
     */
    public function parseFile(string $filepath, array $options = array()) : bool
    {
        $this->checkFileAccess($filepath);
        $internalErrorHandler = libxml_use_internal_errors(true);
        $this->xml = simplexml_load_file($filepath);
        if (false === $this->xml) {
            throw XmlException::xmlerrors(\libxml_get_errors());
        }
        // we set the error handling back to the original
        libxml_use_internal_errors($internalErrorHandler);
        $this->parseXml();
        return true;
    }

    /**
     * Parse the xml file
     *
     * @return void
     */
    protected function parseXml()
    {
        $testSuites = $this->xml->testsuite;
        $statistics = [
            'tests' => 0,
            'assertions' => 0,
            'errors' => 0,
            'failures' => 0,
            'skipped' => 0,
            'time' => 0,
        ];
        foreach ($testSuites as $testSuite) {
            //update the statistics
            $statistics['tests'] = $statistics['tests'] + \intval($testSuite['tests']);
            $statistics['assertions'] = $statistics['assertions'] + \intval($testSuite['assertions']);
            $statistics['errors'] = $statistics['errors'] + \intval($testSuite['errors']);
            $statistics['failures'] = $statistics['failures'] + \intval($testSuite['failures']);
            $statistics['skipped'] = $statistics['skipped'] + \intval($testSuite['skipped']);
            $statistics['time'] = $statistics['time'] + \floatval($testSuite['time']);
            $this->parseTestSuite($testSuite);
        }
        $this->statistics = $statistics;
    }

    /**
     * Parse the testsuite element
     *
     * @param \SimpleXMLElement $testSuite
     */
    protected function parseTestSuite(\SimpleXMLElement $testSuite)
    {
        foreach ($testSuite as $testcase) {
            $testFile = new TestFile();
            $testFile->name($testcase['name']);
            $testFile->file($testcase['file']);
            foreach ($testcase as $case) {
                $testCaseObject = $this->parseTestCase($case);
                $testFile->addTestCase($testCaseObject);
            }
        }
        $this->testFiles[$testSuite['file']] = $testFile;
    }

    /**
     * Parse the xml of a testcase
     *
     * @param \SimpleXMLElement $case
     *
     * @return \TJVB\Testreportmixer\Models\TestCase
     */
    protected function parseTestCase(\SimpleXMLElement $case)
    {
        $testCaseObject = new TestCase();
        $testCaseObject->class($this->getTestCaseCase($case));
        $testCaseObject->classname($this->getTestCaseClassname($case));
        $testCaseObject->name($this->getTestCaseName($case));
        $testCaseObject->line($this->getTestCaseLine($case));
        $testCaseObject->assertions($this->getTestCaseAssertions($case));
        $testCaseObject->duration($this->getTestCaseDuration($case));

        $status = 'successfull';
        if ($case->skipped) {
            $status = 'skipped';
            $testCaseObject->message($case->skipped->__toString());
        }
        if ($case->failure) {
            $status = 'failed';
            $testCaseObject->message($case->failure->__toString());
        }
        if ($case->error) {
            $status = 'error';
            $testCaseObject->message($case->error->__toString());
        }
        $testCaseObject->status($status);
        return $testCaseObject;
    }

    /**
     * Get the case from the testcase element
     *
     * @param \SimpleXMLElement $case
     *
     * @return string
     */
    protected function getTestCaseCase(\SimpleXMLElement $case) : string
    {
        if (isset($case['class'])) {
            return $case['class']->__toString();
        }
        return '';
    }

    /**
     * Get the classname from the testcase element
     *
     * @param \SimpleXMLElement $case
     *
     * @return string
     */
    protected function getTestCaseClassname(\SimpleXMLElement $case) : string
    {
        if (isset($case['classname'])) {
            return $case['classname']->__toString();
        }
        return '';
    }

    /**
     * Get the name from the testcase element
     *
     * @param \SimpleXMLElement $case
     *
     * @return string
     */
    protected function getTestCaseName(\SimpleXMLElement $case) : string
    {
        if (isset($case['name'])) {
            return $case['name']->__toString();
        }
        return '';
    }

    /**
     * Get the line from the testcase element
     *
     * @param \SimpleXMLElement $case
     *
     * @return int
     */
    protected function getTestCaseLine(\SimpleXMLElement $case) : int
    {
        if (isset($case['line'])) {
            return \intval($case['line']->__toString());
        }
        return 0;
    }

    /**
     * Get the assertions from the testcase element
     *
     * @param \SimpleXMLElement $case
     *
     * @return int
     */
    protected function getTestCaseAssertions(\SimpleXMLElement $case) : int
    {
        if (isset($case['assertions'])) {
            return \intval($case['assertions']->__toString());
        }
        return 0;
    }

    /**
     * Get the time from the testcase element
     *
     * @param \SimpleXMLElement $case
     *
     * @return float
     */
    protected function getTestCaseDuration(\SimpleXMLElement $case) : float
    {
        if (isset($case['time'])) {
            return \floatval($case['time']->__toString());
        }
        return 0.0;
    }

    /**
     * Get the number of assertions
     *
     * @return int
     */
    public function assertions() : int
    {
        return (int) $this->statistics['assertions'];
    }

    /**
     * Get the number of errors
     *
     * @return int
     */
    public function errors() : int
    {
        return (int) $this->statistics['errors'];
    }

    /**
     * Get the number of failures
     *
     * @return int
     */
    public function failures() : int
    {
        return (int) $this->statistics['failures'];
    }

    /**
     * Get the number of skipped
     *
     * @return int
     */
    public function skipped() : int
    {
        return (int) $this->statistics['skipped'];
    }

    /**
     * Get the number of tests
     *
     * @return int
     */
    public function tests() : int
    {
        return (int) $this->statistics['tests'];
    }

    /**
     * Get the testfiles
     *
     * @return array
     */
    public function getTestFiles() : array
    {
        return $this->testFiles;
    }
}
