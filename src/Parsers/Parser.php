<?php

namespace TJVB\Testreportmixer\Parsers;

use TJVB\Testreportmixer\Exceptions\FileAccessException;

/**
 * The base parser class, delivers some general functions
 *
 * @author Tobias van Beek <t.vanbeek@tjvb.nl>
 */
abstract class Parser implements ParserInterface
{
    /**
     * Check the read access to a file
     *
     * @param string $filepath
     *
     * @throws FileAccessException
     *
     * @return void
     */
    protected function checkFileAccess(string $filepath)
    {
        if (\is_dir($filepath)) {
            throw FileAccessException::isDirNeedFile($filepath);
        }
        if (!\is_readable($filepath)) {
            throw FileAccessException::noReadAccess($filepath);
        }
    }
}
