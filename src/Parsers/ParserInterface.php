<?php

namespace TJVB\Testreportmixer\Parsers;

use TJVB\Testreportmixer\Exceptions\ParseException;

/**
 * The interface for all the parsers
 *
 * @author Tobias van Beek <t.vanbeek@tjvb.nl>
 */
interface ParserInterface
{
    /**
     * Parse the files
     *
     * @param string $filepath
     * @param array $options
     *
     * @throws ParseException
     */
    public function parseFile(string $filepath, array $options = []) : bool;

    /**
     * Get the number of assertions
     *
     * @return int
     */
    public function assertions() : int;

    /**
     * Get the number of errors test
     *
     * @return int
     */
    public function errors() : int;

    /**
     * Get the number of failures tests
     *
     * @return int
     */
    public function failures() : int;

    /**
     * Get the number of skipped tests
     *
     * @return int
     */
    public function skipped() : int;

    /**
     * Get the number of tests
     *
     * @return int
     */
    public function tests() : int;

    /**
     * Get the testfiles
     *
     * @return array
     */
    public function getTestFiles() : array;
}
