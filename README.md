# TestReportMixer
[![PHP from Packagist](https://img.shields.io/packagist/php-v/tjvb/testreportmixer.svg)](https://packagist.org/packages/tjvb/testreportmixer)
[![Latest Stable Version](https://poser.pugx.org/tjvb/testreportmixer/v/stable)](https://packagist.org/packages/tjvb/testreportmixer)
[![pipeline status](https://gitlab.com/tjvb/testreportmixer/badges/master/pipeline.svg)](https://gitlab.com/tjvb/testreportmixer/commits/master)
[![coverage report](https://gitlab.com/tjvb/testreportmixer/badges/master/coverage.svg)](https://gitlab.com/tjvb/testreportmixer/commits/master)
[![License](https://poser.pugx.org/tjvb/testreportmixer/license)](https://packagist.org/packages/tjvb/testreportmixer)

## Why
The target of TestReportMixer is to convert test result file formats to other formats and / or to combine files to one file.

## How to use
`convert <outputformat> <outputfile> <inputformat> <inputfiles> (<inputfiles>)...`
The input format and outputformat options are described in [Suported Formats](#suported-formats)


## Installation
### global
You can install TestReportMixer global with composer with the command: `composer global require tjvb/testreportmixer` 

### local
You can install TestReportMixer localy with composer with the command: `composer require tjvb/testreportmixer` 

## Suported formats

### Input
- junit

### Output
- junit
- Test Anything Protocol ([TAP](https://testanything.org/))

## Changelog
We (try to) document all the changes in [CHANGELOG](CHANGELOG.md) so read it for more information.

## Contributing
You are very welcome to contribute, read about it in [CONTRIBUTING](CONTRIBUTING.md)

## Code of Conduct
We have a code of conduct, and suspect everybody who want to involve in this project to respect it. [CODE OF CONDUCT](CODE-OF-CONDUCT.md)

## License
The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
